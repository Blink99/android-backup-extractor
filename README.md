Android backup extractor
========================

Utility to extract and repack Android backups created with ```adb backup``` (ICS+). 
Largely based on BackupManagerService.java from AOSP.
Usage: 

java -jar abe.jar pack|unpack [parameters]

	unpack:	abe.sh unpack <backup.ab> <backup.tar> [password]
	pack:	abe.sh pack <backup.tar> <backup.ab> [password]

If you don't specify a password the backup archive won't be encrypted but 
only compressed. 

More details about the backup format and the tool implementation in the 
associated blog post: 

http://nelenkov.blogspot.com/2012/06/unpacking-android-backups.html

